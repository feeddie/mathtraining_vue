import Vue from 'vue'
import App from './App.vue'

import AppStartScreen from './components/StartScreen'
import AppQuestion from './components/Question'
import AppResultScreen from './components/ResultScreen'
import AppMessage from './components/Message'

Vue.component('app-start-screen', AppStartScreen);
Vue.component('app-question', AppQuestion);
Vue.component('app-result-screen', AppResultScreen);
Vue.component('app-message', AppMessage);

new Vue({
  el: '#app',
  render: h => h(App)
})
